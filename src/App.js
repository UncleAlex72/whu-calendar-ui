import React from 'react';
import './App.css';

import Season from "./containers/Season";
import Header from "./components/Header";

const App = () => {
    return (
        <div>
            <Header title={"Hola"}/>
            <div style={{height: "100px"}}/>
            <Season/>
        </div>
    )
};

export default App;
