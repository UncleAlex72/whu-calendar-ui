import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';

const Game = ({game}) => {
    const opponents = game.opponents;
    return (
        <div>
            <Hidden smDown>
                <Grid item md={3}/>
                <Grid item md={6}>
                    <Paper>{opponents} (6)</Paper>
                </Grid>
                <Grid item md={3}/>
            </Hidden>
            <Hidden mdUp>
                <Grid item xs={12}>
                    <Paper>{opponents} (12)</Paper>
                </Grid>
            </Hidden>
        </div>
    )
};

Game.propTypes = {
    game: PropTypes.shape({
        opponents: PropTypes.string.isRequired
    }).isRequired
};

export default Game;