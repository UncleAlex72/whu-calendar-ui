import React from 'react';
import PropTypes from 'prop-types';
import Game from './Game'

const Games = ({games}) => (
    <div>
        {games.map((game, index) => (
            <Game key={index} game={game}/>
        ))}
    </div>
);

Games.propTypes = {
    games: PropTypes.arrayOf(PropTypes.shape(Game.PropTypes)).isRequired
};

export default Games;