import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/Toolbar';

const Header = ({title}) => (
    <AppBar>
        <ToolBar>
            {title}
        </ToolBar>
    </AppBar>
);

Header.propTypes = {
    title: PropTypes.string.isRequired
};

export default Header;