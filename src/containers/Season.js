import { connect } from 'react-redux';
import Games from '../components/Games'

const mapStateToSeason = state => {
    return { games: state.games };
};

const Season = connect(mapStateToSeason)(Games);

export default Season;