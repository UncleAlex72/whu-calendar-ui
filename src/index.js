import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from "react-redux";
import {createStore, applyMiddleware} from "redux";
import {reducer} from "./reducers";
import {fetchSeason, requestSeason} from "./actions";
import thunkMiddleware from 'redux-thunk'

const initialState = {
    games: [],
    title: "Salchichas"
};

const store = createStore(reducer, initialState, applyMiddleware(thunkMiddleware));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

store.dispatch(fetchSeason());