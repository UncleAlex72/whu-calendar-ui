import {RECEIVE_SEASON} from "./actions";

export function reducer(state = {}, action) {
    if (action.type === RECEIVE_SEASON) {
        return {
            ...state,
            games: action.games,
            title: action.title
        }
    }
    else {
        return state;
    }
}