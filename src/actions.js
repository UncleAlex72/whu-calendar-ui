import fetch from 'cross-fetch';

export const REQUEST_SEASON = 'REQUEST_SEASON';
export function requestSeason() {
    return {
        type: REQUEST_SEASON
    }
}

export const RECEIVE_SEASON = 'RECEIVE_SEASON';
export function receiveSeason(json) {
    return {
        type: RECEIVE_SEASON,
        games: json.games,
        title: json.title
    }
}

export function fetchSeason() {
    return function(dispatch) {
        dispatch(requestSeason());
        return fetch("/data.json")
            .then(response => response.json())
            .then(json =>
                dispatch(receiveSeason(json)));
    }
}